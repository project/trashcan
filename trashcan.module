<?php

/**
 * @file
 * Contains trashcan hooks.
 */

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\trashcan\TrashcanEntityPurger;

/**
 * Implements hook_entity_access().
 */
function trashcan_entity_access(EntityInterface $entity, $operation, AccountInterface $account) {
  /** @var \Drupal\trashcan\TrashcanSettingsHandler $trashcan_settings */
  $trashcan_settings = \Drupal::service('trashcan.settings_handler');
  // If this is a trasheable entity, the delete operation shouldn't be used.
  // We should be using Trash, Restore and Purge instead.
  if (in_array($entity->getEntityTypeId(), $trashcan_settings->getAffectedEntityTypes())
    && $operation === 'delete') {
    return AccessResult::forbidden('Trasheable entities should not be deleted. They should be purged instead.');
  }
  return AccessResult::neutral();
}

/**
 * Implements hook_entity_base_field_info().
 */
function trashcan_entity_base_field_info(EntityTypeInterface $entity_type) {
  /** @var \Drupal\trashcan\TrashcanSettingsHandler $trashcan_settings */
  $trashcan_settings = \Drupal::service('trashcan.settings_handler');
  // Add our trash basefields to all affected entities.
  $fields = [];
  if (in_array($entity_type->id(), $trashcan_settings->getAffectedEntityTypes())) {
    $fields['trash_status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('In trash'))
      ->setDescription(t('A flag indicating whether this entity has been moved to the trash bin.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(FALSE);
    $fields['trash_timestamp'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Trash date'))
      ->setDescription(t('The last time this entity was moved to the trash bin.'))
      ->setRequired(FALSE)
      ->setRevisionable(TRUE);
  }
  return $fields;
}

/**
 * Implements hook_entity_view_alter().
 */
function trashcan_entity_view_alter(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display) {
  /** @var \Drupal\trashcan\TrashcanSettingsHandler $trashcan_settings */
  $trashcan_settings = \Drupal::service('trashcan.settings_handler');
  if (!in_array($entity->getEntityTypeId(), $trashcan_settings->getAffectedEntityTypes())) {
    return;
  }
  // Add a visual indicator when entities are in trash.
  // For now the suffix "(In Trash)" is added to the entity label, and the class
  // "trashcan-trashed" is added to the main entity element.
  if (\Drupal::service('trashcan.handler')->isInTrash($entity)) {
    $label_key = $entity->getEntityType()->getKey('label');
    // @todo Find a better way of doing this.
    if (!empty($build[$label_key][0]['#context']['value'])) {
      $build[$label_key][0]['#context']['value'] .= ' ' . t('(In Trash)');
    }
    $build['#attributes']['class'][] = 'trashcan-trashed';
    $build['#attached']['library'][] = 'trashcan/trashed';
  }
}

/**
 * Implements hook_entity_operation().
 */
function trashcan_entity_operation(EntityInterface $entity) {
  /** @var \Drupal\trashcan\TrashcanSettingsHandler $trashcan_settings */
  $trashcan_settings = \Drupal::service('trashcan.settings_handler');
  $operations = [];
  $entity_type_id = $entity->getEntityTypeId();
  if (!in_array($entity_type_id, $trashcan_settings->getAffectedEntityTypes())) {
    return $operations;
  }

  if (!\Drupal::service('trashcan.handler')->isInTrash($entity)
    && $entity->access('update')) {
    $operations["trashcan_{$entity_type_id}_trash"] = [
      'title' => t('Move to Trash'),
      'url' => new Url("entity.{$entity_type_id}.trash", [
        $entity_type_id => $entity->id(),
      ]),
      'weight' => 102,
    ];
  }
  elseif (\Drupal::service('trashcan.handler')->isInTrash($entity)
    && $entity->access('update')) {
    $operations["trashcan_{$entity_type_id}_restore"] = [
      'title' => t('Restore'),
      'url' => new Url("entity.{$entity_type_id}.restore", [
        $entity_type_id => $entity->id(),
      ]),
      'weight' => 103,
    ];
  }
  if (\Drupal::service('trashcan.handler')->isInTrash($entity)
    && \Drupal::currentUser()->hasPermission("purge $entity_type_id entities")) {
    $operations["trashcan_{$entity_type_id}_purge"] = [
      'title' => t('Purge'),
      'url' => new Url("entity.{$entity_type_id}.purge", [
        $entity_type_id => $entity->id(),
      ]),
      'weight' => 104,
    ];
  }

  return $operations;
}

/**
 * No linked trashed items & message.
 *
 * Ensures no trashed items ended up being linked.
 * Also shows a meaningful message to the user.
 */
function trashcan_no_links_to_trash_stuff($element, FormStateInterface $form_state, $form) {
  /** @var \Drupal\trashcan\TrashcanSettingsHandler $trashcan_settings */
  $trashcan_settings = \Drupal::service('trashcan.settings_handler');

  $parsed_url = UrlHelper::parse($element['#value']);
  $is_external = UrlHelper::isExternal($element['#value']);
  if (!$parsed_url['path'] || $is_external) {
    return;
  }

  // On Behat tests, the value for path comes without a / at beginning.
  // See feature: Link Collections show descriptions as expected.
  // However, on manual testing, this value has / at beginning.
  // So, we add a slash at the start, then ensure there is only 1.
  $parsed_url['path'] = str_replace('//', '/', '/' . $parsed_url['path']);

  try {
    $url = Url::fromUri('internal:' . $parsed_url['path']);
  }
  catch (\InvalidArgumentException $exception) {
    // Invalid arguments may be given, not part of this validation.
    return;
  }

  try {
    $route_params = $url->getRouteParameters();
  }
  catch (\UnexpectedValueException $exception) {
    // Any URI may be given, not part of this validation.
    return;
  }

  $entity_type = array_keys($route_params)[0];
  $id = $route_params[$entity_type];
  try {
    $storage = \Drupal::entityTypeManager()->getStorage($entity_type);
  }
  catch (\Exception $e) {
    // Sometimes it can be relative paths that are not entities,
    // like a route to a view, those cases we don't want to validate.
    return;
  }

  $entity = $storage->load($id);

  if (!in_array($entity_type, $trashcan_settings->getAffectedEntityTypes())) {
    return;
  }

  if ((bool) $entity->trash_status->value) {
    $form_state->setError($element, t('Trashed elements are not allowed to be linked "%label%" [%entity%/%id%]',
    ['%entity%' => $entity_type, '%id%' => $id, '%label%' => $entity->label()]));
  }
}

/**
 * Iterates a form structure to find linkit fields.
 */
function trashcan_search_field_in_form(&$form) {
  if (!is_array($form)) {
    return;
  }

  $keys = array_keys($form);
  if (in_array('#element_validate', $keys)) {
    if (in_array('#type', $keys, TRUE) && $form['#type'] === 'linkit') {
      $form['#element_validate'][] = 'trashcan_no_links_to_trash_stuff';
    }
  }

  $children = Element::children($form);
  foreach ($children as $key) {
    trashcan_search_field_in_form($form[$key]);
  }
}

/**
 * Implements hook_form_alter().
 */
function trashcan_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  trashcan_search_field_in_form($form);
}

/**
 * Implements hook_cron().
 */
function trashcan_cron() {
  $now = \Drupal::time()->getRequestTime();
  $next_purge_timestamp = \Drupal::state()->get(TrashcanEntityPurger::NEXT_PURGE_TIMESTAMP);
  /** @var \Drupal\trashcan\TrashcanSettingsHandler $trashcan_settings */
  $trashcan_settings = \Drupal::service('trashcan.settings_handler');

  if (empty($next_purge_timestamp) || $now > $next_purge_timestamp) {
    foreach ($trashcan_settings->getAffectedEntityTypes() as $entityTypeId) {
      \Drupal::service('trashcan.entity_purger_helper')->executePurgeQueue($entityTypeId);
    }
    // Store current date +1 day timestamp.
    $next = strtotime('+1 day', $now);
    \Drupal::state()->set(TrashcanEntityPurger::NEXT_PURGE_TIMESTAMP, $next);
  }

}

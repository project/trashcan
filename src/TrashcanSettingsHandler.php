<?php

namespace Drupal\trashcan;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * TrashcanSettingsHandler service class.
 *
 * This is a helper service to make easier working with the trashcan settings.
 */
class TrashcanSettingsHandler {

  use StringTranslationTrait;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityBundleInfo;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager;

  /**
   * The last installed schema definitions.
   *
   * @var \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface
   */
  protected EntityLastInstalledSchemaRepositoryInterface $entityLastInstalledSchemaRepository;

  /**
   * Constructs a TrashcanSettingsHandler object.
   *
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $entity_definition_update_manager
   *   The entity definition update manager.
   * @param \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface $entity_last_installed_schema_repository
   *   The last installed schema repository service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_bundle_info
   *   The entity type bundle info service.
   */
  public function __construct(EntityDefinitionUpdateManagerInterface $entity_definition_update_manager, EntityLastInstalledSchemaRepositoryInterface $entity_last_installed_schema_repository, ConfigFactoryInterface $config_factory, MessengerInterface $messenger, EntityTypeBundleInfoInterface $entity_bundle_info) {
    $this->entityDefinitionUpdateManager = $entity_definition_update_manager;
    $this->entityLastInstalledSchemaRepository = $entity_last_installed_schema_repository;
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->entityBundleInfo = $entity_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public function isEntityTypeSupported(EntityTypeInterface $entity_type) : bool {
    return is_subclass_of($entity_type->getStorageClass(), SqlEntityStorageInterface::class);
  }

  /**
   * {@inheritdoc}
   */
  public function isEntityTypeEnabled(EntityTypeInterface $entity_type) : bool {
    $enabled_entity_types = $this->getAffectedEntityTypes();
    return in_array($entity_type->id(), $enabled_entity_types, TRUE);
  }

  /**
   * Entity type IDs that should be considered "Trasheable".
   */
  public function getAffectedEntityTypes(): array {
    return $this->configFactory->get('trashcan.settings')->get('enabled') ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function enableEntityType(EntityTypeInterface $entity_type) {
    $field_storage_definitions = $this->entityLastInstalledSchemaRepository->getLastInstalledFieldStorageDefinitions($entity_type->id());

    if (!$this->isEntityTypeSupported($entity_type)) {
      throw new \InvalidArgumentException("Trashcan integration can not be enabled for the {$entity_type->id()} entity type.");
    }

    if (isset($field_storage_definitions['trash_status'])) {
      if ($field_storage_definitions['trash_status']->getProvider() !== 'trashcan') {
        throw new \InvalidArgumentException("The {$entity_type->id()} entity type already has a 'trash_status' field.");
      }
      else {
        throw new \InvalidArgumentException("Trashcan integration is already enabled for the {$entity_type->id()} entity type.");
      }
    }

    $storage_definitions['trash_status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('In trash'))
      ->setDescription(t('A flag indicating whether this entity has been moved to the trash bin.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(FALSE);
    $storage_definitions['trash_timestamp'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Trash date'))
      ->setDescription(t('The last time this entity was moved to the trash bin.'))
      ->setRequired(FALSE)
      ->setRevisionable(TRUE);

    foreach ($storage_definitions as $id => $storage_definition) {
      $this->entityDefinitionUpdateManager->installFieldStorageDefinition($id, $entity_type->id(), 'trashcan', $storage_definition);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function disableEntityType(EntityTypeInterface $entity_type) {
    $field_storage_definitions = $this->entityLastInstalledSchemaRepository->getLastInstalledFieldStorageDefinitions($entity_type->id());
    if (isset($field_storage_definitions['trash_status'])) {
      $this->entityDefinitionUpdateManager->uninstallFieldStorageDefinition($field_storage_definitions['trash_status']);
    }
    if (isset($field_storage_definitions['trash_timestamp'])) {
      $this->entityDefinitionUpdateManager->uninstallFieldStorageDefinition($field_storage_definitions['trash_timestamp']);
    }
  }

}

<?php

namespace Drupal\trashcan;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * TrashcanHandler service class.
 *
 * This is a helper service to make it easier to work with the trashcan flag
 * associated with entities.
 */
class TrashcanHandler {

  use StringTranslationTrait;

  /**
   * The workflow state to use when a moderatable entity is moved into Trash.
   *
   * Note that it is assumed that all users able to move entities to Trash
   * should also be able to transition entities into this state.
   *
   * @todo This could be configurable.
   */
  const TRASHED_WORKFLOW_STATE = 'archived';

  /**
   * The workflow state to use when a moderatable entity is restored from Trash.
   *
   * Note that it is assumed that all users able to restore entities from Trash
   * should also be able to transition entities into this state.
   *
   * @todo This could be configurable.
   */
  const RESTORED_WORKFLOW_STATE = 'draft';

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityBundleInfo;

  /**
   * Moderation Information service, when Content Moderation is enabled.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface|false
   */
  protected $contentModerationInfo = FALSE;

  /**
   * Constructs a TrashcanHandler object.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(TimeInterface $time, MessengerInterface $messenger, ModuleHandlerInterface $module_handler) {
    $this->time = $time;
    $this->messenger = $messenger;
    $this->moduleHandler = $module_handler;
    if ($this->moduleHandler->moduleExists('content_moderation')) {
      // Since we can't inject the service unless the module is enabled, we are
      // calling \Drupal::service() here. This violates some of the
      // DrupalPractice phpcs checks, so we avoid the phpcs failure by wrapping
      // the offending code with these specific tags.
      /* @codingStandardsIgnoreStart */
      $this->contentModerationInfo = \Drupal::service('content_moderation.moderation_information');
      /* @codingStandardsIgnoreEnd */
    }
  }

  /**
   * Whether this entity is in the trash bin.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity we are querying for.
   *
   * @return bool
   *   TRUE if this entity is in trash, FALSE otherwise.
   */
  public function isInTrash(ContentEntityInterface $entity) {
    return (bool) $entity->trash_status->value;
  }

  /**
   * Helper to set the trash flag on a given entity.
   *
   * This method will:
   *  - Set the trash flag status to TRUE.
   *  - Set the trash timestamp value to the current request time.
   *  - Unpublish the entity.
   *
   * If the entity is affected by a content moderation workflow, the "Unpublish"
   * step is done by moving the entity to the workflow state defined in
   * self::TRASHED_WORKFLOW_STATE.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity we are dealing with.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The same entity, after having some values modified. Note that this
   *   method will NOT save this entity, which is the caller's responsibility.
   */
  public function moveToTrash(ContentEntityInterface $entity) {
    $entity->trash_status->value = TRUE;
    $entity->trash_timestamp->value = $this->time->getRequestTime();
    if ($this->contentModerationInfo && $this->contentModerationInfo->isModeratedEntity($entity)) {
      /** @var \Drupal\workflows\WorkflowInterface $workflow */
      $workflow = $this->contentModerationInfo->getWorkflowForEntity($entity);
      $states = $workflow->getTypePlugin()->getStates();
      if (!array_key_exists(self::TRASHED_WORKFLOW_STATE, $states)) {
        $this->messenger->addError($this->t('Could not move entity to trash state %state since it does not exist.', [
          '%state' => self::TRASHED_WORKFLOW_STATE,
        ]));
      }
      else {
        $entity->moderation_state->value = self::TRASHED_WORKFLOW_STATE;
      }
    }
    elseif ($entity instanceof EntityPublishedInterface) {
      $entity->setUnpublished();
    }
    return $entity;
  }

  /**
   * Helper to erase the trash flag on a given entity.
   *
   * This method will:
   *  - Set the trash flag status to FALSE.
   *  - Set the trash timestamp value to NULL.
   *  - Unpublish the entity.
   *
   * If the entity is affected by a content moderation workflow, the "Unpublish"
   * step is done by moving the entity to the workflow state defined in
   * self::RESTORED_WORKFLOW_STATE.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity we are dealing with.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The same entity, after having some values modified. Note that this
   *   method will NOT save this entity, which is the caller's responsibility.
   */
  public function restoreFromTrash(ContentEntityInterface $entity) {
    $entity->trash_status->value = FALSE;
    $entity->trash_timestamp->value = NULL;
    if ($this->contentModerationInfo && $this->contentModerationInfo->isModeratedEntity($entity)) {
      /** @var \Drupal\workflows\WorkflowInterface $workflow */
      $workflow = $this->contentModerationInfo->getWorkflowForEntity($entity);
      $states = $workflow->getTypePlugin()->getStates();
      if (!array_key_exists(self::RESTORED_WORKFLOW_STATE, $states)) {
        $this->messenger->addError($this->t('Could not move entity to restored state %state since it does not exist.', [
          '%state' => self::RESTORED_WORKFLOW_STATE,
        ]));
      }
      else {
        $entity->moderation_state->value = self::RESTORED_WORKFLOW_STATE;
      }
    }
    elseif ($entity instanceof EntityPublishedInterface) {
      $entity->setUnpublished();
    }
    return $entity;
  }

  /**
   * Retrieve the timestamp when this entity was last moved to trash.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity we are dealing with.
   *
   * @return int|null
   *   The timestamp when this entity was last moved to trash, or NULL if the
   *   entity is not in trash.
   */
  public function getTrashTimestamp(ContentEntityInterface $entity) {
    return !empty($entity->trash_timestamp->value) ? $entity->trash_timestamp->value : NULL;
  }

}

<?php

namespace Drupal\trashcan;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\State\StateInterface;

/**
 * Implements TrashcanEntityPurger class.
 */
class TrashcanEntityPurger {

  /**
   * Stores the timestamp to run the next purge process.
   */
  const NEXT_PURGE_TIMESTAMP = 'trashcan_next_purge_timestamp';

  /**
   * Time allowed in Trash Bin.
   */
  const PURGE_TIME_AGO = '3 months ago';

  /**
   * Number of Drupal nodes to purge at each purge worker call.
   */
  const PURGE_BATCH_SIZE = 50;

  /**
   * The name of the queue that holds local items to be purged.
   *
   * @see \Drupal\trashcan\Plugin\QueueWorker\TrashcanEntityPurgeWorker
   */
  const PURGE_QUEUE_NAME = 'trashcan_entity_purge_queue';

  /**
   * The Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The Queue Factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelFactoryInterface $logger_factory,
    TimeInterface $time,
    QueueFactory $queue_factory,
    StateInterface $state
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger_factory->get('trashcan');
    $this->time = $time;
    $this->queueFactory = $queue_factory;
    $this->state = $state;
  }

  /**
   * Populate the purge queue, with items to be deleted from Drupal.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function executePurgeQueue(string $entity_type_id): void {
    $now = $this->time->getCurrentTime();
    $datetime = strtotime(self::PURGE_TIME_AGO, $now);

    $queue = $this->queueFactory->get(self::PURGE_QUEUE_NAME);

    $ids = $this->entityTypeManager
      ->getStorage($entity_type_id)
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('trash_status', 1)
      ->condition('trash_timestamp', $datetime, '<')
      ->execute();

    $chunks = array_chunk($ids, self::PURGE_BATCH_SIZE);

    foreach ($chunks as $chunk) {
      $queue->createItem([
        'batch' => $chunk,
        'entity_type_id' => $entity_type_id,
      ]);
    }
  }

}

<?php

namespace Drupal\trashcan;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions for each trasheable entity type.
 */
class TrashcanPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The trashcan settings handler.
   *
   * @var \Drupal\trashcan\TrashcanSettingsHandler
   */
  protected $trashcanSettings;

  /**
   * TrashcanPermissions constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\trashcan\TrashcanSettingsHandler $trashcan_settings
   *   The trashcan settings handler.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TrashcanSettingsHandler $trashcan_settings) {
    $this->entityTypeManager = $entity_type_manager;
    $this->trashcanSettings = $trashcan_settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('trashcan.settings_handler')
    );
  }

  /**
   * Returns an array of trasheable entity type permissions.
   *
   * @return array
   *   The entity type permissions.
   *
   * @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function entityTypePermissions() {
    $perms = [];
    $entity_types = $this->entityTypeManager->getDefinitions();
    // Generate media permissions for all trasheable entity types.
    foreach ($this->trashcanSettings->getAffectedEntityTypes() as $entity_type_id) {
      $perms += [
        "trash $entity_type_id entities" => [
          'title' => $this->t('Move %type entities to the Trashcan', [
            '%type' => $entity_types[$entity_type_id]->getLabel(),
          ]),
          'description' => $this->t('Users with this permission will be able to move entities to the trashcan. This eventually could make entities being deleted from the system.'),
          'restrict access' => TRUE,
        ],
        "purge $entity_type_id entities" => [
          'title' => $this->t('Purge %type entities from the Trashcan', [
            '%type' => $entity_types[$entity_type_id]->getLabel(),
          ]),
          'description' => $this->t('Users with this permission will be able to permanently delete (purge) trashed entities from the system.'),
          'restrict access' => TRUE,
        ],
      ];
    }
    return $perms;
  }

}

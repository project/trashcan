<?php

namespace Drupal\trashcan\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\trashcan\TrashcanSettingsHandler;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for content entity trash routes.
 */
class TrashcanRouteSubscriber extends RouteSubscriberBase {

  /**
   * The Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The trashcan settings handler.
   *
   * @var \Drupal\trashcan\TrashcanSettingsHandler
   */
  protected $trashcanSettings;

  /**
   * Constructs a TrashRouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\trashcan\TrashcanSettingsHandler $trashcan_settings
   *   The trashcan settings handler.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TrashcanSettingsHandler $trashcan_settings) {
    $this->entityTypeManager = $entity_type_manager;
    $this->trashcanSettings = $trashcan_settings;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // For each trasheable entity type, we will alter the entity routes to add
    // new trash-related ones: trash, restore and purge. These are going to be
    // what our local tasks created in TrashLocalTasks will point to.
    foreach ($this->getTrasheableEntityTypes() as $entity_type_id => $entity_type) {
      if ($canonical = $entity_type->getLinkTemplate('canonical')) {
        $options = [
          '_admin_route' => TRUE,
          // Since the route itself doesn't have the entity_type name as
          // parameter, we pass it as a specially named param here, so it can
          // be retrieved by controllers from the route match. For an example
          // of this being used, see
          // GaTrashConfirmFormBase::getEntityFromRouteMatch().
          '_trashcan_entity_type_id' => $entity_type_id,
          'parameters' => [
            $entity_type_id => [
              'type' => 'entity:' . $entity_type_id,
            ],
          ],
        ];

        // Add the "Move to trash" route.
        $trash_route = new Route(
          $canonical . '/trash',
          [
            '_form' => '\Drupal\trashcan\Form\TrashForm',
            '_title' => 'Move to Trash',
          ],
          [
            '_custom_access' => '\Drupal\trashcan\Form\TrashForm::access',
          ],
          $options
        );
        $collection->add("entity.$entity_type_id.trash", $trash_route);

        // Add the "Restore" route.
        $restore_route = new Route(
          $canonical . '/restore',
          [
            '_form' => '\Drupal\trashcan\Form\RestoreForm',
            '_title' => 'Restore',
          ],
          [
            '_custom_access' => '\Drupal\trashcan\Form\RestoreForm::access',
          ],
          $options
        );
        $collection->add("entity.$entity_type_id.restore", $restore_route);

        // Add the "Purge" route.
        $purge_route = new Route(
          $canonical . '/purge',
          [
            '_form' => '\Drupal\trashcan\Form\PurgeForm',
            '_title' => 'Purge',
          ],
          [
            '_custom_access' => '\Drupal\trashcan\Form\PurgeForm::access',
          ],
          $options
        );
        $collection->add("entity.$entity_type_id.purge", $purge_route);
      }
    }
  }

  /**
   * Returns the list of Trasheable entity types.
   *
   * @return \Drupal\Core\Entity\ContentEntityTypeInterface[]
   *   An array of entity types indexed by their IDs. Returns an empty array
   *   if no matching entities are found.
   */
  protected function getTrasheableEntityTypes() {
    $entity_types = $this->entityTypeManager->getDefinitions();
    $return = [];
    foreach ($this->trashcanSettings->getAffectedEntityTypes() as $entity_type_id) {
      if (!empty($entity_types[$entity_type_id])) {
        $return[$entity_type_id] = $entity_types[$entity_type_id];
      }
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', 95];
    return $events;
  }

}

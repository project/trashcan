<?php

namespace Drupal\trashcan\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\trashcan\TrashcanHandler;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Permanently purges an entity.
 *
 * This would be unnecessary if we used the built-in "Delete" action for really
 * removing entities from the system. However, since we have overridden the
 * controller so "Delete" links/tabs/actions don't unexpectedly show up on the
 * site, we provide this specific action so users with appropriate permission
 * can perform bulk purging when needed.
 *
 * Note the "confirm" annotation parameter is a VBO specific tag that allows us
 * to use the built-in confirmation step. More information can be found on VBO
 * readme file.
 *
 * @Action(
 *   id = "trashcan_purge_action",
 *   label = @Translation("Purge"),
 *   confirm = TRUE,
 * )
 */
class TrashcanPurgeAction extends ViewsBulkOperationsActionBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The trash handler service.
   *
   * @var \Drupal\trashcan\TrashcanHandler
   */
  protected $trashHandler;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, TrashcanHandler $trash_handler, Messenger $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->trashHandler = $trash_handler;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('trashcan.handler'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    if (!$entity) {
      $this->messenger->addError($this->t('The purge action cannot purge an empty entity.'));
    }
    $this->entityTypeManager
      ->getStorage($entity->getEntityTypeId())
      ->delete([$entity]);
    $this->messenger->addStatus($this->t('The entity %label was purged from the system.', [
      '%label' => $entity->toLink()->toString(),
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $object */
    if ($account->hasPermission("purge {$object->getEntityTypeId()} entities")
      && $this->trashHandler->isInTrash($object)) {
      $result = AccessResult::allowed();
    }
    else {
      $result = AccessResult::forbidden('The logged-in user does not have permission to purge this entity, or the entity is not in Trash.');
    }
    return $return_as_object ? $result : $result->isAllowed();
  }

}

<?php

namespace Drupal\trashcan\Plugin\Action;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\trashcan\TrashcanHandler;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Restores an entity from trash.
 *
 * Note the "confirm" annotation parameter is a VBO specific tag that allows us
 * to use the built-in confirmation step. More information can be found on VBO
 * readme file.
 *
 * @Action(
 *   id = "trashcan_restore_from_trash_action",
 *   label = @Translation("Restore from Trash"),
 *   confirm = TRUE,
 * )
 */
class TrashcanRestoreFromTrashAction extends ViewsBulkOperationsActionBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The trash handler service.
   *
   * @var \Drupal\trashcan\TrashcanHandler
   */
  protected $trashHandler;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, TrashcanHandler $trash_handler, Messenger $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->trashHandler = $trash_handler;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('trashcan.handler'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $this->trashHandler->restoreFromTrash($entity);
    $entity->save();
    $this->messenger->addStatus($this->t('The entity %label was restored from Trash.', [
      '%label' => $entity->toLink()->toString(),
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\Core\Entity\EntityInterface $object */
    $result = $object->access('update', $account, TRUE);
    return $return_as_object ? $result : $result->isAllowed();
  }

}

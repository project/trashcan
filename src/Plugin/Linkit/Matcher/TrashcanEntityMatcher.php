<?php

namespace Drupal\trashcan\Plugin\Linkit\Matcher;

use Drupal\linkit\Plugin\Linkit\Matcher\EntityMatcher;

/**
 * Provides default linkit matchers for all entity types (not in trash).
 *
 * @Matcher(
 *   id = "trashcan_entity",
 *   label = @Translation("Entity"),
 *   deriver = "\Drupal\trashcan\Plugin\Derivative\TrashcanEntityMatcherDeriver"
 * )
 */
class TrashcanEntityMatcher extends EntityMatcher {

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $summary = parent::getSummary();
    $summary[] = $this->t('Only matching entities that are not in trash (not configurable).');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($search_string) {
    $query = parent::buildEntityQuery($search_string);
    $query->condition('trash_status', FALSE);
    return $query;
  }

}

<?php

namespace Drupal\trashcan\Plugin\Linkit\Matcher;

use Drupal\linkit\Plugin\Linkit\Matcher\NodeMatcher;

/**
 * Provides specific linkit matchers for the node entity type.
 *
 * @Matcher(
 *   id = "trashcan_entity:node",
 *   label = @Translation("Content - not trashed"),
 *   target_entity = "node",
 *   provider = "node"
 * )
 */
class TrashcanNodeMatcher extends NodeMatcher {

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $summary = parent::getSummary();
    $summary[] = $this->t('Only matching nodes that are not in trash (not configurable).');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($search_string) {
    $query = parent::buildEntityQuery($search_string);
    $query->condition('trash_status', FALSE);
    return $query;
  }

}

<?php

namespace Drupal\trashcan\Plugin\views\wizard;

use Drupal\Core\Form\FormStateInterface;
use Drupal\media\Plugin\views\wizard\Media;

/**
 * TrashcanMedia views wizard class.
 *
 * This class is not a plugin per se, but it is used instead of the class for
 * the plugin defined in \Drupal\media\Plugin\views\wizard\Media, once we want
 * to customize its behavior, while keeping its plugin id.
 */
class TrashcanMedia extends Media {

  /**
   * {@inheritdoc}
   */
  protected function defaultDisplayFilters($form, FormStateInterface $form_state) {
    $filters = parent::defaultDisplayFilters($form, $form_state);

    // We want to have a default behavior that media entities "in trash" should
    // not show up in any newly created view, out-of-the-box. In order to
    // accomplish that, we add this filter here, so if the site builder really
    // wants to show trashed entities in the view they are creating on the UI,
    // they need to explicitly turn this filter off.
    $filters['trash_status'] = [
      'value' => FALSE,
      'table' => 'media_field_data',
      'field' => 'trash_status',
      'plugin_id' => 'boolean',
      'entity_type' => 'media',
      'entity_field' => 'trash_status',
      'id' => 'trash_status',
      'expose' => [
        'operator' => FALSE,
      ],
      'group' => 1,
    ];

    return $filters;
  }

}

<?php

namespace Drupal\trashcan\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\trashcan\TrashcanEntityPurger;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements TrashcanEntityPurgeWorker class.
 *
 * @QueueWorker(
 *   id = "trashcan_entity_purge_queue",
 *   title = @Translation("GA Trash Entity Purge Worker"),
 *   cron = {"time" = 60}
 * )
 */
class TrashcanEntityPurgeWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger interface.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelFactoryInterface $logger_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger_factory->get('trashcan');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $now = \Drupal::time()->getCurrentTime();
    $datetime = strtotime(TrashcanEntityPurger::PURGE_TIME_AGO, $now);

    try {
      $storage = $this->entityTypeManager->getStorage($data['entity_type_id']);

      // Querying once again to validate the entity still exists.
      $ids = $storage
        ->getQuery()
        ->accessCheck(FALSE)
        ->condition('nid', $data['batch'], 'IN')
        ->condition('trash_status', 1)
        ->condition('trash_timestamp', $datetime, '<')
        ->execute();

      if ($ids) {
        $storage->delete($storage->loadMultiple($ids));
        $this->logger->info('Successfully purged @count local items.', ['@count' => count($ids)]);
      }
    }
    catch (\Exception $e) {
      $this->logger->warning("An error prevented purging local items. Error message: {$e->getMessage()}");
    }
  }

}

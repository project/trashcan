<?php

namespace Drupal\trashcan\Plugin\Derivative;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\linkit\Plugin\Derivative\EntityMatcherDeriver;
use Drupal\trashcan\TrashcanSettingsHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides matchers based on active entities that can be in trash.
 *
 * @see plugin_api
 */
class TrashcanEntityMatcherDeriver extends EntityMatcherDeriver {

  use StringTranslationTrait;

  /**
   * The trashcan settings handler.
   *
   * @var \Drupal\trashcan\TrashcanSettingsHandler
   */
  protected $trashcanSettings;

  /**
   * Creates an TrashcanEntityMatcherDeriver object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\trashcan\TrashcanSettingsHandler $trashcan_settings
   *   The trashcan settings handler.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TrashcanSettingsHandler $trashcan_settings) {
    parent::__construct($entity_type_manager);
    $this->trashcanSettings = $trashcan_settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('trashcan.settings_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {

    $this->derivatives = parent::getDerivativeDefinitions($base_plugin_definition);
    $entities_than_can_be_in_trash = array_flip($this->trashcanSettings->getAffectedEntityTypes());
    // For node matcher to avoid nodes in trash see:
    // Drupal\trashcan\Plugin\Linkit\TrashcanNodeMatcher.
    unset($entities_than_can_be_in_trash['node']);
    $this->derivatives = array_intersect_key($this->derivatives, $entities_than_can_be_in_trash);

    foreach ($this->derivatives as &$derivative) {
      $derivative['label'] .= ' - ' . $this->t("not trashed");
    }

    return $this->derivatives;
  }

}

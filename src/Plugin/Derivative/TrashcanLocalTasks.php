<?php

namespace Drupal\trashcan\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\trashcan\TrashcanSettingsHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines our dynamic local tasks.
 */
class TrashcanLocalTasks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The trashcan settings handler.
   *
   * @var \Drupal\trashcan\TrashcanSettingsHandler
   */
  protected $trashcanSettings;

  /**
   * Constructs a TrashcanLocalTasks object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\trashcan\TrashcanSettingsHandler $trashcan_settings
   *   The trashcan settings handler.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TrashcanSettingsHandler $trashcan_settings) {
    $this->entityTypeManager = $entity_type_manager;
    $this->trashcanSettings = $trashcan_settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('trashcan.settings_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];

    foreach ($this->getTrasheableEntityTypes() as $entity_type_id => $entity_type) {
      // Add the local task to all trasheable entities. The access handler will
      // make sure it only shows up when the entity has the correct trash
      // status and the user can perform that operation.
      if ($entity_type->hasLinkTemplate('canonical')) {
        // Add the "Move to Trash" local task.
        $this->derivatives["entity.$entity_type_id.trash"] = [
          'route_name' => "entity.$entity_type_id.trash",
          'title' => $this->t('Move to Trash'),
          'base_route' => "entity.$entity_type_id.canonical",
          'weight' => 98,
        ];
        // Add the "Restore" local task.
        $this->derivatives["entity.$entity_type_id.restore"] = [
          'route_name' => "entity.$entity_type_id.restore",
          'title' => $this->t('Restore'),
          'base_route' => "entity.$entity_type_id.canonical",
          'weight' => 98,
        ];
        // Add the "Purge" local task.
        $this->derivatives["entity.$entity_type_id.purge"] = [
          'route_name' => "entity.$entity_type_id.purge",
          'title' => $this->t('Purge'),
          'base_route' => "entity.$entity_type_id.canonical",
          'weight' => 98,
        ];
      }
    }

    foreach ($this->derivatives as &$entry) {
      $entry += $base_plugin_definition;
    }
    return $this->derivatives;
  }

  /**
   * Returns the list of Trasheable content entity types.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   An array of entity objects indexed by their IDs. Returns an empty array
   *   if no matching entities are found.
   */
  protected function getTrasheableEntityTypes() {
    $entity_types = $this->entityTypeManager->getDefinitions();
    $return = [];
    foreach ($this->trashcanSettings->getAffectedEntityTypes() as $entity_type_id) {
      if (!empty($entity_types[$entity_type_id])) {
        $return[$entity_type_id] = $entity_types[$entity_type_id];
      }
    }
    return $return;
  }

}

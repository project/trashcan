<?php

namespace Drupal\trashcan\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\trashcan\TrashcanSettingsHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Listens to the config save event for trashcan.settings.
 */
class TrashcanConfigSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The Trash manager.
   *
   * @var \Drupal\trashcan\TrashcanSettingsHandler
   */
  protected TrashcanSettingsHandler $trashcanSettings;

  /**
   * The last installed schema definitions.
   *
   * @var \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface
   */
  protected $entityLastInstalledSchemaRepository;

  /**
   * The router builder service.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routerBuilder;

  /**
   * Constructs the TrashConfigSubscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\trashcan\TrashcanSettingsHandler $trashcan_settings
   *   The trashcan settings handler.
   * @param \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface $entity_last_installed_schema_repository
   *   The last installed schema repository service.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $router_builder
   *   The router builder service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    TrashcanSettingsHandler $trashcan_settings,
    EntityLastInstalledSchemaRepositoryInterface $entity_last_installed_schema_repository,
    RouteBuilderInterface $router_builder
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->trashcanSettings = $trashcan_settings;
    $this->entityLastInstalledSchemaRepository = $entity_last_installed_schema_repository;
    $this->routerBuilder = $router_builder;
  }

  /**
   * Enables or disables trash integration for entity types.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The ConfigCrudEvent to process.
   */
  public function onSave(ConfigCrudEvent $event) {
    if ($event->getConfig()->getName() === 'trashcan.settings') {
      $supported_entity_types = array_filter($this->entityTypeManager->getDefinitions(), function ($entity_type) {
        return $this->trashcanSettings->isEntityTypeSupported($entity_type);
      });
      $enabled_entity_types = $event->getConfig()->get('enabled');

      // Work around core bug #2605144, which doesn't provide the original
      // config data on import, only on regular save.
      // @see https://www.drupal.org/project/drupal/issues/2605144
      foreach ($supported_entity_types as $entity_type_id => $entity_type) {
        $field_storage_definitions = $this->entityLastInstalledSchemaRepository->getLastInstalledFieldStorageDefinitions($entity_type_id);

        // Enable trash integration for the requested entity types.
        if (in_array($entity_type_id, $enabled_entity_types, TRUE) && !isset($field_storage_definitions['trash_status'])) {
          $this->trashcanSettings->enableEntityType($entity_type);
        }

        // Disable trashcan integration for the rest of the entity types.
        if (!in_array($entity_type_id, $enabled_entity_types, TRUE)
          && isset($field_storage_definitions['trash_status'])
          && $field_storage_definitions['trash_status']->getProvider() === 'trashcan') {
          $this->trashcanSettings->disableEntityType($entity_type);
        }
      }

      // When an entity type is enabled or disabled, the router needs to be
      // rebuilt to add the corresponding tabs in the trash UI.
      $this->routerBuilder->setRebuildNeeded();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::SAVE][] = ['onSave'];
    return $events;
  }

}

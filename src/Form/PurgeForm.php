<?php

namespace Drupal\trashcan\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Provides a entity purge confirmation form.
 */
class PurgeForm extends TrashcanConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trashcan_purge_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to purge %label?', ['%label' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('<p>This operation cannot be undone.</p>');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Purge');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $this->entityTypeManager
      ->getStorage($entity->getEntityTypeId())
      ->delete([$entity]);

    $this->messenger()->addStatus($this->t('The @entity %label has been purged.', [
      '@entity' => $entity->getEntityType()->get('label'),
      '%label' => $entity->label(),
    ]));

    $form_state->setRedirectUrl($this->getRedirectUrl());
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirectUrl() {
    $entity = $this->getEntity();
    if ($entity && $entity->hasLinkTemplate('collection')) {
      return $entity->toUrl('collection');
    }
    else {
      return new Url('<front>');
    }
  }

  /**
   * Access handler for this form.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   A RouteMatch object.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(RouteMatchInterface $route_match) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntityFromRouteMatch($route_match);
    if (\Drupal::currentUser()->hasPermission("purge {$entity->getEntityTypeId()} entities")
      && $this->trashHandler->isInTrash($entity)) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden('The logged-in user does not have permission to purge this entity, or the entity is not in Trash.');
  }

}

<?php

namespace Drupal\trashcan\Form;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\trashcan\TrashcanHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a base confirmation form for trashcan forms, with some common logic.
 */
abstract class TrashcanConfirmFormBase extends ConfirmFormBase {

  /**
   * The content entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $entity;

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Trash Handler service.
   *
   * @var \Drupal\trashcan\TrashcanHandler
   */
  protected $trashHandler;

  /**
   * The Class Resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\trashcan\TrashcanHandler $trash_handler
   *   The Trashcan Handler service.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The Class Resolver.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TrashcanHandler $trash_handler, ClassResolverInterface $class_resolver) {
    $this->entityTypeManager = $entity_type_manager;
    $this->trashHandler = $trash_handler;
    $this->classResolver = $class_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('trashcan.handler'),
      $container->get('class_resolver')
    );
  }

  /**
   * Fetches the entity this form refers to.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The content entity affected by this form.
   */
  public function getEntity() {
    if (!empty($this->entity)) {
      return $this->entity;
    }
    $this->entity = $this->getEntityFromRouteMatch();
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $entity = $this->getEntity();
    if ($entity && $entity->hasLinkTemplate('collection')) {
      return $entity->toUrl('collection');
    }
    elseif ($entity) {
      return $entity->toUrl();
    }
    else {
      return new Url('<front>');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    if (!$entity) {
      throw new NotFoundHttpException('Could not retrieve entity affected by this form.');
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Returns the url object for redirect path.
   *
   * @return \Drupal\Core\Url
   *   The redirect URL object.
   */
  protected function getRedirectUrl() {
    return $this->getCancelUrl();
  }

  /**
   * Retrieves an entity from route match.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   (optional) A RouteMatch object, in case the current route should not be
   *   considered for this. Defaults to NULL (i.e. consider the current route).
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity object as determined from the passed-in route match.
   */
  protected function getEntityFromRouteMatch(RouteMatchInterface $route_match = NULL) {
    $route_match = $route_match ?? $this->getRouteMatch();
    $parameter_name = $route_match->getRouteObject()->getOption('_trashcan_entity_type_id');
    return $route_match->getParameter($parameter_name);
  }

}

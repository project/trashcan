<?php

namespace Drupal\trashcan\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Provides a trash confirmation form.
 */
class TrashForm extends TrashcanConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trashcan_trash_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to move %label to Trash?', ['%label' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('<p>The @entity %label will be moved to the Trash bin and become unpublished.<br><strong>Note</strong>: After 90 days this content automatically will be purged from the Trash bin.</p>', [
      '@entity' => $this->entity->getEntityType()->get('label'),
      '%label' => $this->entity->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Move to Trash');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $build = parent::buildForm($form, $form_state);
    $entity = $this->getEntity();

    // If this entity being trashed has usages associated with it, inform the
    // user that things might look broken.
    $usage_build = [];
    try {
      $entity_usage_controller = $this->classResolver->getInstanceFromDefinition('\Drupal\entity_usage\Controller\ListUsageController');
      $usage_build = $entity_usage_controller->listUsagePage($entity->getEntityTypeId(), $entity->id());
    }
    catch (\Exception $e) {
    }
    if (!empty($usage_build[0]['#rows'])) {
      $build['description']['#weight'] = 10;
      $build['actions']['#actions'] = 100;

      $build['entity_usage_trash_warning'] = [
        '#theme' => 'status_messages',
        '#message_list' => [
          'warning' => [$this->t('The content items below seem to be using or linking to this content. Moving this to the trash could cause broken links if there is live content linking to it.')],
        ],
        '#status_headings' => ['warning' => $this->t('Warning message')],
        '#weight' => 20,
      ];
      $build['usage_table'] = $usage_build;
      $build['usage_table']['#weight'] = 30;
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $this->trashHandler->moveToTrash($entity);
    $entity->save();
    $this->messenger()->addStatus($this->t('The @entity <a href=":entity-href">%label</a> has been moved to the trash. <a href=":undo-page">Undo</a>', [
      '@entity' => $entity->getEntityType()->getLabel(),
      ':entity-href' => $entity->toUrl()->toString(),
      '%label' => $entity->label(),
      ':undo-page' => Url::fromRoute("entity.{$entity->getEntityTypeId()}.restore", [
        $entity->getEntityTypeId() => $entity->id(),
      ])->toString(),
    ]));
    $form_state->setRedirectUrl($this->getRedirectUrl());
  }

  /**
   * Access handler for this form.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   A RouteMatch object.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(RouteMatchInterface $route_match) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntityFromRouteMatch($route_match);
    if ($entity && $entity->access('update') && \Drupal::currentUser()->hasPermission("trash {$entity->getEntityTypeId()} entities") &&
        !$this->trashHandler->isInTrash($entity)) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden('The logged-in user does not have permission to move this entity to Trash, or it may already be trashed.');
  }

}

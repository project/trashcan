<?php

namespace Drupal\trashcan\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Provides a entity restore confirmation form.
 */
class RestoreForm extends TrashcanConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trashcan_restore_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to restore %label?', ['%label' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('<p>The @entity %label will be restored from Trash.</p>', [
      '@entity' => $this->entity->getEntityType()
        ->get('label'),
      '%label' => $this->entity->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Restore');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $this->trashHandler->restoreFromTrash($entity);
    $entity->save();
    $this->messenger()->addStatus($this->t('The @entity @label has been restored. You may want to publish it manually now.', [
      '@entity' => $entity->getEntityType()->get('label'),
      '@label' => $entity->label(),
    ]));
    $form_state->setRedirectUrl($entity->toUrl());
  }

  /**
   * Access handler for this form.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   A RouteMatch object.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(RouteMatchInterface $route_match) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntityFromRouteMatch($route_match);
    if ($entity && $entity->access('update')
      && $this->trashHandler->isInTrash($entity)) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden('The logged-in user does not have permission to restore this entity from Trash.');
  }

}

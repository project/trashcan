<?php

namespace Drupal\Tests\trashcan\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test the trashcan basic functionality on nodes.
 *
 * @group trashcan
 */
class TrashcanNodeTest extends BrowserTestBase {

  /**
   * A user with permission to trash content but not restoring.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUser;

  /**
   * A user with permission to trash, restore and purge the trashcan.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['block', 'node', 'trashcan'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create Basic page node type.
    if ($this->profile != 'standard') {
      $this->drupalCreateContentType([
        'type' => 'page',
        'name' => 'Basic page',
        'display_submitted' => FALSE,
      ]);
    }

    $this->webUser = $this->drupalCreateUser([
      'access content',
      'create page content',
      'edit own page content',
      'delete own page content',
    ]);
    $this->adminUser = $this->drupalCreateUser([
      'access content',
      'create page content',
      'edit own page content',
      'delete own page content',
      'edit any page content',
      'delete any page content',
      'administer nodes',
      'bypass node access',
      'trash node entities',
      'purge node entities',
    ]);
    $this->drupalPlaceBlock('local_tasks_block', ['id' => 'page_tabs_block']);
    $this->drupalPlaceBlock('local_actions_block', ['id' => 'page_actions_block']);
  }

  /**
   * Test moving a node to the trashcan and restoring it.
   */
  public function testTrashAndRestoreNode() {
    // Login as a regular user.
    $this->drupalLogin($this->webUser);

    // Create "Basic page" content with title.
    $settings = [
      'title' => $this->randomMachineName(8),
    ];
    $node = $this->drupalCreateNode($settings);

    // Load the node edit form.
    $this->drupalGet('node/' . $node->id() . '/edit');

    // Make sure the task is there.
    // @todo We want a explicit permission.
    $this->assertSession()->linkNotExists('Move to Trash');

    // Now edit the same node as an admin user.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('node/' . $node->id() . '/edit');

    // Make sure we can Move to the trash.
    $this->assertSession()->linkExists('Move to Trash');

    // Make sure the link works as expected.
    $this->clickLink('Move to Trash');
    $this->assertSession()->addressEquals('node/1/trash');

    $this->submitForm([], 'Move to Trash');

    // The content has been moved to the trash.
    $this->assertSession()->addressEquals('node/' . $node->id());
    $this->assertSession()->statusMessageContains('The Content ' . $node->getTitle() . ' has been moved to the trash', 'status');
    $this->assertSession()->titleEquals($node->getTitle() . ' (In Trash) | Drupal');
    $this->assertSession()->elementExists('css', 'article.trashcan-trashed');

    // I'm not authorized to see the node anymore with a regular editor.
    $this->drupalLogin($this->webUser);
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(403);

    // I can restore the content.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->linkExists('Restore');
    $this->clickLink('Restore');
    $this->submitForm([], 'Restore');
    $this->assertSession()->statusMessageContains('The Content ' . $node->getTitle() . ' has been restored. You may want to publish it manually now.', 'status');

    // The value is still unpublished after restoring.
    $this->clickLink('Edit');
    $this->assertTrue($this->getSession()->getPage()->hasUncheckedField('status[value]'));
  }

  /**
   * Test I cannot directly delete a trasheable entity.
   */
  public function testDeletingNodeIsAvoided() {
    $this->drupalLogin($this->webUser);

    // Create "Basic page" content with title.
    $settings = [
      'title' => $this->randomMachineName(8),
    ];
    $node = $this->drupalCreateNode($settings);

    // Load the node edit form.
    $this->drupalGet('node/' . $node->id() . '/edit');

    // Verify I cannot delete it.
    $this->assertSession()->linkNotExists('Delete');

    $this->drupalGet('node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test moving a node to the trashcan and restoring it.
   */
  public function testPurgingNode() {
    // Login as a privileged user.
    $this->drupalLogin($this->adminUser);

    // Create "Basic page" content with title.
    $settings = [
      'title' => $this->randomMachineName(8),
    ];
    $node = $this->drupalCreateNode($settings);

    // Load the node edit form.
    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->clickLink('Move to Trash');
    $this->submitForm([], 'Move to Trash');

    // The content has been moved to the trash.
    $this->assertSession()->addressEquals('node/' . $node->id());
    $this->assertSession()->statusMessageContains('The Content ' . $node->getTitle() . ' has been moved to the trash', 'status');

    // Make sure we can Purge.
    $this->assertSession()->linkExists('Purge');

    // Make sure the link works as expected.
    $this->clickLink('Purge');
    $this->assertSession()->addressEquals('node/1/purge');
    $this->submitForm([], 'Purge');

    $this->assertSession()->statusMessageContains('The Content ' . $node->getTitle() . ' has been purged.', 'status');

    // Make sure the content is now deleted.
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(404);
  }

}

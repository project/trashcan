<?php

namespace Drupal\Tests\trashcan\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test the trashcan settings form.
 *
 * @group trashcan
 */
class TrashcanSettingsTest extends BrowserTestBase {

  /**
   * A user with permission to trash, restore and purge the trashcan.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['block', 'node', 'trashcan'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create Basic page node type.
    if ($this->profile != 'standard') {
      $this->drupalCreateContentType([
        'type' => 'page',
        'name' => 'Basic page',
        'display_submitted' => FALSE,
      ]);
    }

    $this->adminUser = $this->drupalCreateUser([
      'access content',
      'create page content',
      'edit own page content',
      'delete own page content',
      'edit any page content',
      'delete any page content',
      'administer nodes',
      'bypass node access',
      'trash node entities',
      'purge node entities',
      'administer trashcan',
    ]);
    $this->drupalPlaceBlock('local_tasks_block', ['id' => 'page_tabs_block']);
    $this->drupalPlaceBlock('local_actions_block', ['id' => 'page_actions_block']);
  }

  /**
   * Test moving a node to the trashcan and restoring it.
   */
  public function testStoringSettings() {
    // Login as a regular user.
    $this->drupalLogin($this->adminUser);

    // Create "Basic page" content with title.
    $settings = [
      'title' => $this->randomMachineName(8),
    ];
    $node = $this->drupalCreateNode($settings);
    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->assertSession()->linkExists('Move to Trash');

    // Load the node edit form.
    $this->drupalGet('admin/config/trashcan/settings');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->titleEquals('Trashcan Settings | Drupal');

    $this->assertSession()->checkboxChecked('enabled_entity_types[entity_types][node]');
    $this->assertSession()->checkboxChecked('enabled_entity_types[entity_types][media]');

    $edit = [
      'enabled_entity_types[entity_types][node]' => FALSE,
      'enabled_entity_types[entity_types][media]' => 'media',
    ];
    $this->submitForm($edit, 'Save configuration');

    $this->assertSession()->checkboxNotChecked('enabled_entity_types[entity_types][node]');
    $this->assertSession()->checkboxChecked('enabled_entity_types[entity_types][media]');

    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->assertSession()->linkNotExists('Move to Trash');
  }

}
